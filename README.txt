INTRODUCTION
------------

Civic Breadcrumbs module replaces core breadcrumbs with configurable breadcrumbs
allowing to select parent page for breadcrumbs per content type. 
For example having a landing page for news item can be the parent page 
for single news item page.
Extra configuration settings permit to edit or hide the homepage 
from breadcrumbs trails.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
-------
Installing the Civic Breadcrumbs module is simple:

1) Copy the civic_breadcrumbs folder to the modules folder in
   your installation

2) Enable the module using Administer -> Extend page (/admin/modules)

3) Configure the breadcrumbs at Configuration -> User interface -> Breadcrumbs

4) Place the default system breadcrumbs block into a region at 
   Structure -> Block Layout


CONFIGURATION
-------
Civic Breadcrumbs module allows you to select parent breadcrumb page per 
content type, show or hide Homepage link, change Homepage link text.
