<?php

namespace Drupal\civic_breadcrumbs\Breadcrumb;

use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Link;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Entity\EntityInterface;

/**
 * Implementation of CivicBreadcrumbBuilder.
 */
class CivicBreadcrumbBuilder implements BreadcrumbBuilderInterface {
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $parameters = $route_match->getParameters()->all();
    if (isset($parameters['node']) && ($parameters['node'] instanceof EntityInterface)) {
      return TRUE;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function build(RouteMatchInterface $route_match) {
    $config = \Drupal::config('breadcrumbs.settings');
    $include_homepage = $config->get('include_homepage');

    $breadcrumb = new Breadcrumb();

    if ($include_homepage) {
      $homepage_title = $config->get('homepage_title') ? $config->get('homepage_title') : 'Home';
      $breadcrumb->addCacheContexts(['route']);
      $breadcrumb->addLink(Link::createFromRoute($homepage_title, '<front>'));
    }

    $node = $route_match->getParameter('node');
    $parentId = $config->get('breadcrumb_parent_' . $node->getType());

    if ($parentId != '') {
      $parentNode = \Drupal::entityTypeManager()->getStorage('node')->load($parentId);
      $breadcrumb->addLink(Link::createFromRoute($parentNode->getTitle(), 'entity.node.canonical', ['node' => $parentId]));
    }

    $breadcrumb->addLink(Link::createFromRoute($node->get('title')->getValue()[0]['value'], '<current>', ['node' => $node->get('nid')->getValue()[0]['value']]));

    $breadcrumb->addCacheContexts(['route']);

    return $breadcrumb;
  }

}
