<?php

namespace Drupal\civic_breadcrumbs\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\NodeType;

/**
 * Configure breadcrumbs settings for this site.
 */
class BreadcrumbsSettingsForm extends ConfigFormBase {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'breadcrumbs.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'breadcrumbs_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    // Details for grouping general settings.
    $details_general = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
    ];

    $details_general['include_homepage'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Include Homepage'),
      '#description' => $this->t('Include the homepage as the first element in the breadcrumb.'),
      '#default_value' => $config->get('include_homepage'),
    ];

    $details_general['homepage_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Homepage'),
      '#description' => $this->t('Text to be displayed as homepage. If empty and "Include Homepage" checkbox is checked, "Home" will be used.'),
      '#default_value' => $config->get('homepage_title'),
    ];

    // Details for grouping content type settings.
    $details_content_types = [
      '#type' => 'details',
      '#title' => $this->t('Content types settings'),
      '#open' => TRUE,
    ];

    $details_content_types['markup'] = [
      '#type' => 'markup',
      '#markup' => t('Select breadcrumb parent page foreach content type. Leave empty if content type has no parent.'),
    ];

    $node_types = NodeType::loadMultiple();

    foreach ($node_types as $node_type) {

      $nodeId = $config->get('breadcrumb_parent_' . $node_type->id());

      $details_content_types['breadcrumb_parent_' . $node_type->id()] = [
        '#type' => 'entity_autocomplete',
        '#target_type' => 'node',
        // '#selection_settings' => [
            // 'target_bundles' => array('page', 'landing_page'),
        // ],
        '#title' => $node_type->label(),
        '#default_value' => $nodeId ? \Drupal::entityTypeManager()->getStorage('node')->load($nodeId) : '',
      ];

    }

    $form[] = $details_general;
    $form[] = $details_content_types;

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $breadcrumbsConfig = $this->configFactory->getEditable(static::SETTINGS);

    $breadcrumbsConfig->set('homepage_title', $form_state->getValue('homepage_title'));
    $breadcrumbsConfig->set('include_homepage', $form_state->getValue('include_homepage'));
    $breadcrumbsConfig->set('current_page_as_link', $form_state->getValue('current_page_as_link'));

    $node_types = NodeType::loadMultiple();
    foreach ($node_types as $node_type) {
      $breadcrumbsConfig->set('breadcrumb_parent_' . $node_type->id(), $form_state->getValue('breadcrumb_parent_' . $node_type->id()));
    }
    $breadcrumbsConfig->save();
    parent::submitForm($form, $form_state);
  }

}
